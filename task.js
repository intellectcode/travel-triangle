 var userData = {}; //Nirav : store current user and their todo

 function saveDetail(userForm) {
     var user_name = userForm.elements['txtName'].value;
     if (!userData[user_name]) { //Nirav : User  doesn't exist, will add it first
         userData[user_name] = [];
     }
     userData[user_name].push({
         todo: userForm.elements['txtTodo'].value,
         assign: userForm.elements['txtAssign'].value
     })


     var list_element = document.getElementById("list");
     var append_element = "";
     list_element.innerHTML = "";

     Object.keys(userData).map((e) => {
         append_element = "<li> <p id='" + e + "' > " + e + ": <button type='button' onclick='sortTask(this)'> Sort </button> </p>";

         userData[e].map((record) => {
             append_element +=
                 "<p>" + record.assign + " assigned " + record.todo + " to him</p>"
         })
         append_element += "</li>"
         list_element.innerHTML = list_element.innerHTML + append_element;
     })
     userForm.elements['txtTodo'].value = userForm.elements['txtAssign'].value = "";
 }


 //Nirav : Create New user
 function createNewUser(userForm) {
     var user_name = userForm.elements['txtName'].value
     if (userData[user_name]) { //Nirav : checks user exist or not
         alert("User with name " + user_name + " already exist");
     } else {
         userData[user_name] = [];
         alert("User with name " + user_name + " succesfully added");
     }
 }

 function sortTask(list_obj) {

     var li_parent = list_obj.parentElement.parentElement;
     var append_element = "<p id='" + list_obj.parentElement.id + "' > " + list_obj.parentElement.id + ": <button type='button' onclick='sortTask(this)'> Sort </button> </p>";
     var temp_arr = userData[list_obj.parentElement.id];
     //Nirav : Sort the array depending on Todo
     temp_arr.sort((record1, record2) => {
         return ((record1["todo"] < record2["todo"]) ? -1 : ((record1["todo"] > record2["todo"]) ? 1 : 0));
     })

     temp_arr.map((e) => {
         append_element +=
             "<p>" + e.assign + " assigned " + e.todo + " to him</p>"
     })
     li_parent.innerHTML = append_element; //Nirav : Replace the innerHTML with sorted created list
 }
